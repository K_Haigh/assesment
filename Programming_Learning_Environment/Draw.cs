﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Learning_Environment
{

    class Draw : I_Draw
    {
        //Instance variables
        private Graphics canvass;
        private Pen pen;
        private int x_ord = 150, y_ord = 150;
        private Boolean filling;

        public Draw (Graphics component)
        {
            canvass = component;
            Clear();
            //x_ord = 0;
            //y_ord = 0;
        }

        public void Circle(int radius)
        {
            canvass.DrawEllipse(pen, x_ord, y_ord, radius, radius);
        }

        public void Clear()
        {
            canvass.Clear(Color.White);
        }

        public void DrawTo(int x_ord, int y_ord)
        {
            //Debugging
            //pen = new Pen( Color.FromName("Black") );
            Console.WriteLine("4) " + pen);
            Console.WriteLine("5) " + getPen() );

            canvass.DrawLine(pen, x_ord, y_ord, this.x_ord, this.y_ord);
            MoveTo(this.x_ord, this.y_ord);
        }

        public void MoveTo(int x_ord, int y_ord)
        {
            //Debugging
            Console.WriteLine("1.X) " + this.x_ord);
            Console.WriteLine("1.Y)" + this.x_ord);

            this.x_ord = x_ord;
            this.y_ord = y_ord;

            //Debugging
            Console.WriteLine("2.X) " + this.x_ord);
            Console.WriteLine("2.Y)" + this.x_ord);
        }

        public void Polygon(int points, List<KeyValuePair<int, int>> coordinates)
        {
            int corners = coordinates.Count;
            int count = 0;
            Point[] vertices = new Point[corners];
            
            foreach (KeyValuePair<int, int> vertex in coordinates)
            {
                vertices[count] = new Point (coordinates[count].Key, coordinates[count].Value);
            }

            canvass.DrawPolygon(pen, vertices);
        }

        public void Rectangle(int a_side, int b_side)
        {
            canvass.DrawRectangle(pen, x_ord, y_ord, a_side, b_side);
        }

        public void Reset()
        {
            Clear();
            x_ord = 0;
            y_ord = 0;
        }

        public void SetColour(Color colour)
        {
            //debugging
            Console.WriteLine("1) " + colour);
            Console.WriteLine("2) " + pen);

            pen = new Pen(colour);
            filling = false;

            //debugging
            Console.WriteLine("3) " + pen);
        }

        //debugging
        public Pen getPen()
        {
            return pen;
        }

        public void SetFill(bool toFill)
        {
            filling = toFill;
        }

        
        //Right-angled triangle, width and height
        public void Triangle(int width, int height)
        {
            Point[] abc = new Point[3];
            abc[0] = new Point(x_ord, y_ord);
            abc[1] = new Point(x_ord + width, y_ord);
            abc[2] = new Point(x_ord, y_ord + height);

            canvass.DrawPolygon(pen, abc);
        }
        //Triangle with a horizontal base, two side lengths and an interior angle
        public void Triangle(int a_side, int b_side, int angle)
        {
            // !Note! - not sure about the maths on this one... Practically check

            Point[] abc = new Point[3];
            abc[0] = new Point(x_ord, y_ord);
            abc[1] = new Point(x_ord + a_side, y_ord);
            double radianAngle = angle / Math.PI;


            // c = (a^2 + b^2 -2abCos(C))^1/2
            double c_side = Math.Sqrt(a_side * a_side + b_side * b_side - 2 * a_side * b_side * Math.Cos(radianAngle));


            int cX = (int)Math.Round(x_ord + b_side * Math.Cos(Math.PI - radianAngle));
            int cY = (int)Math.Round(y_ord + b_side * Math.Sin(Math.PI - radianAngle));
            abc[2] = new Point(cY, cX);


            canvass.DrawPolygon(pen, abc);
        }
    }
}
