﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Programming_Learning_Environment
{
    class Interpreter : I_Interpreter
    {
        private Draw output;
        char[] delimitCommand = new char[] { ' ' };
        char[] delimitData = new char[] { ',' };

        public Interpreter(Graphics component)
        {
            output = new Draw(component);
        }



        public string CheckCommands(string[] linesIn)
        {
            throw new NotImplementedException();
        }

        public string CheckParameters(string[] linesIn)
        {
            throw new NotImplementedException();
        }



        public void InterpretLine(string lineIn)
        {
            string instruction = lineIn.Split(delimitCommand, 2)[0];
            string data = lineIn.Split(delimitCommand, 2)[1];

            string[] dataPointsString;
            int[] dataPoints;
            int count;


            switch (instruction)
            {
                case "moveto":
                    dataPointsString = data.Split(delimitData);
                    dataPoints = new int[dataPointsString.Length];
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    output.MoveTo(dataPoints[0], dataPoints[1]);
                    break;

                case "drawto":
                    dataPointsString = data.Split(delimitData);

                    dataPoints = new int[dataPointsString.Length];
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(datum.Trim());
                        count++;
                    }

                    output.DrawTo(dataPoints[0], dataPoints[1]);
                    break;

                case "setcolour":
                    output.SetColour( getColour(data.Trim()) );
                    break;

                case "setfill":
                    //Implement
                    break;

                case "circle":
                    output.Circle(Int32.Parse(data.Trim()));
                    break;

                case "triangle":
                    dataPointsString = data.Split(delimitData);
                    dataPoints = new int[dataPointsString.Length];
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int16.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    if (dataPoints.Length == 2)
                    {
                        output.Triangle(dataPoints[0], dataPoints[1]);
                    }
                    else if (dataPoints.Length == 2)
                    {
                        output.Triangle(dataPoints[0], dataPoints[1], dataPoints[2]);
                    }

                    break;

                case "rectangle":
                    dataPointsString = data.Split(delimitData);
                    dataPoints = new int[dataPointsString.Length];
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int16.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    output.Rectangle(dataPoints[0], dataPoints[1]);
                    break;

                case "polygon":
                    dataPointsString = data.Split(delimitData);
                    dataPoints = new int[dataPointsString.Length];
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int16.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    List<KeyValuePair<int, int>> listVertices = new List<KeyValuePair<int, int>>();
                    count = 1;
                    while (count <= dataPoints.Length)
                    {
                        listVertices.Add(new KeyValuePair<int, int>(dataPoints[count], dataPoints[count + 1]));
                        count += 2;
                    }

                    output.Polygon(dataPoints[0], listVertices);
                    break;

                case "clear":
                    output.Clear();
                    break;

                case "reset":
                    output.Reset();
                    break;
            }
        }

        private Color getColour(string colourIn)
        {
            switch (colourIn)
            {
                case "aliceblue":
                    return Color.FromName("AliceBlue");

                case "antiquewhite":
                    return Color.FromName("AntiqueWhite");

                case "aqua":
                    return Color.FromName("Aqua");

                case "aquamarine":
                    return Color.FromName("Aquamarine");

                case "azure":
                    return Color.FromName("Azure");

                case "beige":
                    return Color.FromName("Beige");

                case "bisque":
                    return Color.FromName("Bisque");

                case "black":
                    return Color.FromName("Black");

                case "blanchedalmond":
                    return Color.FromName("BlanchedAlmond");

                case "blue":
                    return Color.FromName("blue");

                case "blueviolet":
                    return Color.FromName("BlueViolet");

                case "brown":
                    return Color.FromName("Brown");

                case "burlyWood":
                    return Color.FromName("BurlyWood");

                case "cadetblue":
                    return Color.FromName("CadetBlue");

                case "chartreuse":
                    return Color.FromName("Chartreuse");

                case "chocolate":
                    return Color.FromName("Chocolate");

                case "coral":
                    return Color.FromName("Coral");

                case "cornflowerblue":
                    return Color.FromName("CornflowerBlue");

                case "cornsilk":
                    return Color.FromName("Cornsilk");

                case "crimson":
                    return Color.FromName("Crimson");

                case "cyan":
                    return Color.FromName("Cyan");

                case "darkblue":
                    return Color.FromName("DarkBlue");

                case "darkcyan":
                    return Color.FromName("DarkCyan");

                case "darkgoldenrod":
                    return Color.FromName("DarkGoldenrod");

                case "darkgray":
                    return Color.FromName("DarkGray");

                case "darkgreen":
                    return Color.FromName("DarkGreen");

                case "darkkhaki":
                    return Color.FromName("DarkKhaki");

                case "darkmagenta":
                    return Color.FromName("DarkMagenta");

                case "darkolivegreen":
                    return Color.FromName("DarkOliveGreen");

                case "darkorange":
                    return Color.FromName("DarkOrange");

                case "darkorchid":
                    return Color.FromName("DarkOrchid");

                case "darkred":
                    return Color.FromName("DarkRed");

                case "darksalmon":
                    return Color.FromName("DarkSalmon");

                case "darkseagreen":
                    return Color.FromName("DarkSeaGreen");

                case "darkslateblue":
                    return Color.FromName("DarkSlateBlue");

                case "darkslategray":
                    return Color.FromName("DarkSlateGray");

                case "darkturquoise":
                    return Color.FromName("DarkTurquoise");

                case "darkviolet":
                    return Color.FromName("DarkViolet");

                case "deeppink":
                    return Color.FromName("DeepPink");

                case "deepskyblue":
                    return Color.FromName("DeepSkyBlue");

                case "dimgray":
                    return Color.FromName("DimGray");

                case "dodgerblue":
                    return Color.FromName("DodgerBlue");

                case "firebrick":
                    return Color.FromName("Firebrick");

                case "floralwhite":
                    return Color.FromName("FloralWhite");

                case "forestgreen":
                    return Color.FromName("ForestGreen");

                case "fuchsia":
                    return Color.FromName("Fuchsia");

                case "gainsboro":
                    return Color.FromName("Gainsboro");

                case "ghostwhite":
                    return Color.FromName("GhostWhite");

                case "gold":
                    return Color.FromName("Gold");

                case "goldenrod":
                    return Color.FromName("Goldenrod");

                case "gray":
                    return Color.FromName("Gray");

                case "green":
                    return Color.FromName("Green");

                case "greenyellow":
                    return Color.FromName("GreenYellow");

                case "honeydew":
                    return Color.FromName("Honeydew");

                case "hotpink":
                    return Color.FromName("HotPink");

                case "indianred":
                    return Color.FromName("IndianRed");

                case "indigo":
                    return Color.FromName("Indigo");

                case "ivory":
                    return Color.FromName("Ivory");

                case "khaki":
                    return Color.FromName("Khaki");

                case "lavender":
                    return Color.FromName("Lavender");

                case "lavenderblush":
                    return Color.FromName("LavenderBlush");

                case "lawngreen":
                    return Color.FromName("LawnGreen");

                case "lemonchiffon":
                    return Color.FromName("LemonChiffon");

                case "lightblue":
                    return Color.FromName("LightBlue");

                case "lightcoral":
                    return Color.FromName("LightCoral");

                case "lightcyan":
                    return Color.FromName("LightCyan");

                case "lightgoldenrodyellow":
                    return Color.FromName("LightGoldenrodYellow");

                case "lightgray":
                    return Color.FromName("LightGray");

                case "lightgreen":
                    return Color.FromName("LightGreen");

                case "lightpink":
                    return Color.FromName("LightPink");

                case "lightsalmon":
                    return Color.FromName("LightSalmon");

                case "lightseaGreen":
                    return Color.FromName("LightSeaGreen");

                case "lightskyblue":
                    return Color.FromName("LightSkyBlue");

                case "lightslategray":
                    return Color.FromName("LightSlateGray");

                case "lightsteelblue":
                    return Color.FromName("LightSteelBlue");

                case "lightyellow":
                    return Color.FromName("LightYellow");

                case "lime":
                    return Color.FromName("Lime");

                case "limegreen":
                    return Color.FromName("LimeGreen");
                
                case "linen":
                    return Color.FromName("Linen");

                case "magenta":
                    return Color.FromName("Magenta");

                case "maroon":
                    return Color.FromName("Maroon");

                case "mediumaquamarine":
                    return Color.FromName("MediumAquamarine");

                case "mediumblue":
                    return Color.FromName("MediumBlue");

                case "mediumorchid":
                    return Color.FromName("MediumOrchid");

                case "mediumpurple":
                    return Color.FromName("MediumPurple");

                case "mediumseagreen":
                    return Color.FromName("MediumSeaGreen");

                case "mediumslateblue":
                    return Color.FromName("MediumSlateBlue");

                case "mediumspringgreen":
                    return Color.FromName("MediumSpringGreen");

                case "mediumturquoise":
                    return Color.FromName("MediumTurquoise");

                case "mediumvioletred":
                    return Color.FromName("MediumVioletRed");

                case "midnightblue":
                    return Color.FromName("MidnightBlue");

                case "mintcream":
                    return Color.FromName("MintCream");

                case "mistyrose":
                    return Color.FromName("MistyRose");

                case "moccasin":
                    return Color.FromName("Moccasin");

                case "navajowhite":
                    return Color.FromName("NavajoWhite");

                case "navy":
                    return Color.FromName("Navy");

                case "oldlace":
                    return Color.FromName("OldLace");

                case "olive":
                    return Color.FromName("Olive");

                case "olivedrab":
                    return Color.FromName("OliveDrab");

                case "orange":
                    return Color.FromName("Orange");

                case "orangeRed":
                    return Color.FromName("OrangeRed");

                case "orchid":
                    return Color.FromName("Orchid");

                case "palegoldenrod":
                    return Color.FromName("PaleGoldenrod");

                case "palegreen":
                    return Color.FromName("PaleGreen");

                case "paleturquoise":
                    return Color.FromName("PaleTurquoise");

                case "palevioletRed":
                    return Color.FromName("PaleVioletRed");

                case "papayawhip":
                    return Color.FromName("PapayaWhip");

                case "peachpuff":
                    return Color.FromName("PeachPuff");

                case "peru":
                    return Color.FromName("Peru");

                case "pink":
                    return Color.FromName("Pink");

                case "plum":
                    return Color.FromName("Plum");

                case "powderblue":
                    return Color.FromName("PowderBlue");

                case "purple":
                    return Color.FromName("Purple");

                case "red":
                    return Color.FromName("Red");

                case "rosybrown":
                    return Color.FromName("RosyBrown");

                case "royalblue":
                    return Color.FromName("RoyalBlue");

                case "saddlebrown":
                    return Color.FromName("SaddleBrown");

                case "salmon":
                    return Color.FromName("Salmon");

                case "sandybrown":
                    return Color.FromName("SandyBrown");

                case "seagreen":
                    return Color.FromName("SeaGreen");

                case "seashell":
                    return Color.FromName("SeaShell");

                case "sienna":
                    return Color.FromName("Sienna");

                case "silver":
                    return Color.FromName("Silver");

                case "skyblue":
                    return Color.FromName("SkyBlue");

                case "slateblue":
                    return Color.FromName("SlateBlue");

                case "slategray":
                    return Color.FromName("SlateGray");

                case "snow":
                    return Color.FromName("Snow");

                case "springgreen":
                    return Color.FromName("SpringGreen");

                case "steelblue":
                    return Color.FromName("SteelBlue");

                case "tan":
                    return Color.FromName("Tan");

                case "teal":
                    return Color.FromName("Teal");

                case "thistle":
                    return Color.FromName("Thistle");

                case "tomato":
                    return Color.FromName("Tomato");

                case "transparent":
                    return Color.FromName("Transparent");

                case "turquoise":
                    return Color.FromName("Turquoise");

                case "violet":
                    return Color.FromName("Violet");

                case "wheat":
                    return Color.FromName("Wheat");

                case "white":
                    return Color.FromName("White");

                case "whitesmoke":
                    return Color.FromName("WhiteSmoke");

                case "yellow":
                    return Color.FromName("Yellow");

                case "yellowgreen":
                    return Color.FromName("YellowGreen");

                //No colours any more, I want them painted black.
                default:
                    return Color.FromName("Black");

            }
        }
    }
}
