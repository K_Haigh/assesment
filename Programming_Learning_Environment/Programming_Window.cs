﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    public partial class Programming_Window : Form
    {
        public Programming_Window()
        {
            InitializeComponent();
        }

        private void CmdLine_Click(object sender, EventArgs e)
        {
            this.Command_Line.Text = "";
        }

        private void CmdLine_IfBlank(object sender, EventArgs e)
        {
            if ( this.Command_Line.Text.Equals("") )
            {
                this.Command_Line.Text = "[enter command]";
            }
        }

        private void CmdLine_IfEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !this.Command_Line.Text.Equals("") )
            {
                string lineIn = this.Command_Line.Text.ToLower();

                //Function Test 1 - enter 'code' in command line, should run
                Interpreter draw = new Interpreter( Display.CreateGraphics() );
                draw.InterpretLine(lineIn);
                this.Command_Line.Text = "";
            }
        }
    }
}
